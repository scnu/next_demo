import React from "react";
import Grid from '@material-ui/core/Grid';
import { AccessAlarm, ThreeDRotation } from '@material-ui/icons';
import App from "../../components/app/App";
import "./index.less"

export default class _index extends React.Component {

    constructor(props){
        super();
    }

    render() {

        let boxData = [
            [
                {icon:<AccessAlarm></AccessAlarm>,title:"响应式",content:"针对不同屏幕大小设计"},
                {icon:<ThreeDRotation></ThreeDRotation>,title:"主题",content:"可配置的主题满足多样化的品牌"},
                {icon:<AccessAlarm></AccessAlarm>,title:"国际化",content:"内建业界通用的国际化方案"},
            ],
            [
                {icon:<ThreeDRotation></ThreeDRotation>,title:"最佳实践",content:"良好的工程实践助你持续产出高质量的代码"},
                {icon:<ThreeDRotation></ThreeDRotation>,title:"Mock 数据",content:"实用的本地数据调用方案"},
                {icon:<ThreeDRotation></ThreeDRotation>,title:"UI 测试",content:"自动化测试保障前端产品质量"},
            ],
        ]

        return (
            <App className="page-index" title="首页">
                <div className="banner-wrapper">
                    <div className="banner-title-wrapper">
                        <h1>云徙</h1>
                        <p>开箱即用的中台前端/设计解决方案</p>
                    </div>
                    <div className="banner-image-wrapper">

                    </div>
                </div>
                <div className="page-wrapper">
                    <h2>What can <span>Yunxi</span> do for you?</h2>
                    {
                        boxData.map((row,index)=>(
                            <Grid key={index} container>
                                {
                                    row.map((item,idx)=>(
                                        <Grid key={idx} container item justify="center" alignItems="center" xs>
                                            <div className="page-box">
                                                <div>{item.icon}</div>
                                                <h3>{item.title}</h3>
                                                <p>{item.content}</p>
                                            </div>
                                        </Grid>
                                    ))
                                }
                            </Grid>
                        ))
                    }
                </div>
            </App>
        );
    }
}