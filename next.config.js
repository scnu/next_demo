const withLess = require('@zeit/next-less')

module.exports = withLess({
    pageExtensions: ['jsx',"js"],
    webpack(config, options) {
        // Remove minifed react aliases for material-ui so production builds work
        if (config.resolve.alias) {
            delete config.resolve.alias.react
            delete config.resolve.alias['react-dom']
        }
        return config
    }
})