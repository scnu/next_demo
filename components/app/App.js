import Head from 'next/head'
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Search from '@material-ui/icons/Search';
import "./App.less"


import React, { Component } from "react";

export default class App extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className={this.props.className}>
                <Head>
                    <title>{ this.props.title }</title>
                    <meta charSet='utf-8' />
                    <meta name='viewport' content='initial-scale=1.0, width=device-width' />
                </Head>
                <Header/>
                { this.props.children }
                <Footer/>
            </div>
        )
    }
}

export function Header({value=1}) {
    return (
        <header>
            <Grid container>
                <Grid item  xs={2}>
                    云徙
                </Grid>
                <Grid container item xs>
                    <Grid item xs>
                        <TextField
                            id="search"
                            type="search"
                            margin="none"
                            placeholder="搜索"
                            InputProps={{
                                startAdornment:(
                                    <InputAdornment position="start">
                                        <Search />
                                    </InputAdornment>
                                )
                            }}

                        />
                    </Grid>
                    <Grid item style={{width:"220px"}}>
                        <AppBar position="static">
                            <Tabs
                                value={value}
                            >
                                <Tab value="1" label="首页" />
                                <Tab value="2" label="文档" />
                                <Tab value="3" label="组件" />
                            </Tabs>
                        </AppBar>
                    </Grid>
                </Grid>
            </Grid>
        </header>
    );
}

export function Footer() {

    let data = [
        [
            {name:"Ant design",href:"http://www.baidu.com"},
            {name:"Ant design",href:"http://www.baidu.com"},
        ],
        [
            {name:"Ant design",href:"http://www.baidu.com"},
            {name:"Ant design",href:"http://www.baidu.com"},
        ],
        [
            {name:"Ant design",href:"http://www.baidu.com"},
            {name:"Ant design",href:"http://www.baidu.com"},
        ],
        [
            {name:"Ant design",href:"http://www.baidu.com"},
            {name:"Ant design",href:"http://www.baidu.com"},
        ],
    ]

    return (
        <footer className="page_footer">
            <Grid container item xs={12}>
                {data.map((list, index)=> (
                        <Grid key={index} item xs={6} sm={3}>
                            <div className="item">
                                {
                                    list.map((item,index)=>(
                                        <div key={index} >
                                            <div><a href={item.href}>{item.name}</a></div>
                                        </div>
                                    ))
                                }
                            </div>
                        </Grid>
                    ))
                }
            </Grid>
        </footer>
    );
}